import { createAction, props } from '@ngrx/store';
import { IQuery } from 'src/app/forecast/forecast.model';

export const getForecast = createAction(
  '[Forecast] Get Forecast',
  props<{query: IQuery}>()

);

export const getForecastFailure = createAction(
  '[Forecast] Get Forecast Failure',
  props<{error: any}>()
);

export const getForecastSuccess = createAction(
  '[Forecast] Get Forecast Success',
  props<{response: any}>()
);
