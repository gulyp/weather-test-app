import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromForecast from '../reducers/forecast.reducer';

export const selectForecastState = createFeatureSelector<fromForecast.ForecastState>(
    fromForecast.forecastFeatureKey,
);

export const selectForecasts = createSelector(
  selectForecastState,
  (state: fromForecast.ForecastState) => state.forecasts
);

