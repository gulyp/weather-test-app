import { Action, createReducer, on } from '@ngrx/store';
import { IForecast } from 'src/app/forecast/forecast.model';
import * as ForecastAction from '../actions/forecast.actions';

export const forecastFeatureKey = 'forecast';

export interface ForecastState {
    forecasts: IForecast[];
    result: any;
    isLoading: boolean;
    isLoadingSuccess: boolean;
    isLoadingFailure: boolean;
}

export const initialState: ForecastState = {
    forecasts: [],
    result: '',
    isLoading: false,
    isLoadingSuccess: false,
    isLoadingFailure: false
};

export const forecastReducer = createReducer(
    initialState,
    on(ForecastAction.getForecastSuccess,
        (state: ForecastState, { response }) =>
        ({
            ...state,
            forecasts: [...state.forecasts, response]
        }))
);

export function reducer(state: ForecastState | undefined, action: Action): any {
    return forecastReducer(state, action);
}
