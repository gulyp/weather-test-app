import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, exhaustMap, catchError } from 'rxjs/operators';
import { WeatherService } from 'src/app/_services/weather.service';
import * as forecastActions from '../actions/forecast.actions';

@Injectable()
export class ForecastEffects {

  constructor(
    private actions$: Actions,
    private service: WeatherService
  ) {}

  getForecast$ = createEffect(() =>
  this.actions$.pipe(
    ofType(forecastActions.getForecast),
    exhaustMap(action =>
      this.service.getWeatherByCity(action.query.city, action.query.units).pipe(
        map(response => {
          return forecastActions.getForecastSuccess({response})
        }),
        catchError((error: any) => of(forecastActions.getForecastFailure(error))))
    )
  )
  );


}
