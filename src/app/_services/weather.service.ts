import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private rootURL = 'https://api.openweathermap.org/data/2.5/weather?';
  private appKey = '0d7303c17ee3d3482cd82a2ad273a90d';

  constructor(private http: HttpClient) { }

  getWeatherByCity(cityId: number, unit: string) {
    const composedUrl = `${this.rootURL}id=${cityId}&units=${unit}&appid=${this.appKey}`;
    console.log(unit);
    return this.http.get(composedUrl);
  }
}
