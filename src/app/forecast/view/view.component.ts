import { Component, Input, OnInit } from '@angular/core';
import { IForecast } from '../forecast.model';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  @Input() forecast: IForecast | undefined;
  icon: string | undefined;
  constructor() {
   }

  ngOnInit(): void {
    this.icon = `http://openweathermap.org/img/w/${this.forecast?.weather[0].icon}.png`;
  }

}
