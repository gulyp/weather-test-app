import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ForecastState } from 'src/app/store/reducers/forecast.reducer';
import { selectForecasts } from 'src/app/store/selectors/forecast.selector';
import { IForecast } from '../forecast.model';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  forecasts$: Observable<IForecast[]>;

  constructor(private store: Store<ForecastState>) { 
    this.forecasts$ = this.store.pipe(select(selectForecasts)); 
  }

  ngOnInit(): void {
  }

}
