import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForecastAddComponent } from './forecast-add/forecast-add.component';
import { ForecastsComponent } from './forecasts/forecasts.component';
import {StoreModule} from '@ngrx/store';
import { forecastFeatureKey, reducer } from '../store/reducers/forecast.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsComponent } from './forms/forms.component';
import { ViewComponent } from './view/view.component';



@NgModule({
  declarations: [
    ForecastAddComponent,
    ForecastsComponent,
    FormsComponent,
    ViewComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    StoreModule.forFeature(forecastFeatureKey, reducer),
  ],
  exports: [
    ForecastAddComponent,
    ForecastsComponent,
    FormsComponent,
  ]
})

export class ForecastModule { }
