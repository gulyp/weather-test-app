import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import { IForecast } from './../forecast.model';
import { ForecastState } from '../../store/reducers/forecast.reducer';
import { selectForecasts } from '../../store/selectors/forecast.selector';

@Component({
  selector: 'app-forecasts',
  templateUrl: './forecasts.component.html',
  styleUrls: ['./forecasts.component.scss']
})
export class ForecastsComponent implements OnInit {

  forecasts$: Observable<IForecast[]>;
  constructor(private store: Store<ForecastState>) {
    this.forecasts$ = this.store.pipe(select(selectForecasts));
  }
  
  ngOnInit(): void {
  }

}


