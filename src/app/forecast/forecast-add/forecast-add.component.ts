import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ForecastState } from 'src/app/store/reducers/forecast.reducer';
import { IQuery } from '../forecast.model';
import * as forecastActions from '../../store/actions/forecast.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faPlus, IconDefinition } from '@fortawesome/free-solid-svg-icons';

interface ICity {
    "id": number;
    "name": string;
    "state": string;
    "country": string;
    "coord": {
        "lon": number;
        "lat": number;
    }
}
@Component({
  selector: 'app-forecast-add',
  templateUrl: './forecast-add.component.html',
  styleUrls: ['./forecast-add.component.scss']
})
export class ForecastAddComponent implements OnInit {
  queryForm: FormGroup = new FormGroup({});
  cities: ICity[] = [];
  faPlus: IconDefinition = faPlus;
  readOnly = false;
  cityName: string = '';
  constructor(private store: Store<ForecastState>, private fb: FormBuilder, private http: HttpClient) { 
  }
  
  ngOnInit(): void {
    this.http.get<ICity[]>("assets/data/city.list.json").subscribe((data)=> {
      this.cities = data;
    });
    this.initForm();
  }

  initForm(): void {
    this.queryForm = this.fb.group({
      city: [null, [Validators.required]],
      units: ['', [Validators.required, Validators.pattern(/\b(metric|imperial|standard)\b/)]],
      cityName: ['']
    })
  }

  isValidInput(fieldName: string): boolean {
    return this.queryForm.controls[fieldName].invalid &&
      (this.queryForm.controls[fieldName].dirty || this.queryForm.controls[fieldName].touched);
  }


  onSubmit() {
    this.getForecast(this.queryForm.value);
    this.getCityName(+this.queryForm.value.city);
    this.readOnly = !this.readOnly;
  }

  getCityName(id: number) {
    const selected = this.cities.find(city => city.id === id)
    this.cityName = selected?.name || '';
  }

  getForecast(query: IQuery) {
    this.store.dispatch(forecastActions.getForecast({query}));
  }
}
